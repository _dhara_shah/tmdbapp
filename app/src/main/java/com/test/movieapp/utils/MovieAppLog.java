package com.test.movieapp.utils;

import android.util.Log;

import com.test.movieapp.BuildConfig;
import com.test.movieapp.MovieApp;
import com.test.movieapp.di.CoreInjector;

public class MovieAppLog {
    public MovieAppLog() {
        CoreInjector.from(MovieApp.INSTANCE).inject(this);
    }

    public static void v(final String tag, final String msg) {
        if (useLog()) {
            Log.v(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public static void d(final String tag, final String msg) {
        if (useLog()) {
            Log.d(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public static void e(final String tag, final String msg) {
        if (useLog()) {
            Log.e(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public static void wtf(final String tag, final Throwable throwable) {
        if (useLog()) {
            Log.wtf(String.valueOf(tag), throwable);
        }
    }

    private static boolean useLog() {
        return BuildConfig.DEBUG;
    }
}
