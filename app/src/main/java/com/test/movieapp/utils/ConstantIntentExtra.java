package com.test.movieapp.utils;

public class ConstantIntentExtra {
    public static final String EXTRA_MOVIE_ID = "com.movieapp" + "EXTRA_MOVIE_ID";
    public static final String EXTRA_BUNDLE_MOVIE = "com.movieapp" + "EXTRA_BUNDLE_MOVIE";
}
