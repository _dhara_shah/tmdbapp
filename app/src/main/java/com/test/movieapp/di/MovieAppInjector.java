package com.test.movieapp.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.test.movieapp.dagger2.components.MovieComponent;

public class MovieAppInjector {
    private MovieAppInjector() { }

    public static MovieComponent from(@NonNull final Context context) {
        final MovieComponentProvider provider = (MovieComponentProvider) context.getApplicationContext();
        return provider.getMovieComponent(context);
    }
}