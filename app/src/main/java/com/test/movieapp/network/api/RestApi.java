package com.test.movieapp.network.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import com.test.movieapp.network.entity.MovieDetailResponse;
import com.test.movieapp.network.entity.MovieResponse;

public interface RestApi {
    @GET("discover/{discoverType}")
    Call<MovieResponse> getMovies(@Path("discoverType") String discoverType,
                                  @Query("api_key") String apiKey,
                                  @Query("page") int page,
                                  @Query("sort_by") String sortBy,
                                  @Query("release_date.lte") String releaseDate);

    @GET("movie/{movie_id}")
    Call<MovieDetailResponse> getMovieDetails(@Path("movie_id") long movieId,
                                              @Query("api_key") String apiKey);
}
