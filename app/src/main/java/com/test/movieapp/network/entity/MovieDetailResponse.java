package com.test.movieapp.network.entity;

import com.google.gson.annotations.SerializedName;

public class MovieDetailResponse {
    @SerializedName("backdrop_path")
    private String backdropPath;
    @SerializedName("original_title")
    private String title;
    @SerializedName("overview")
    private String overview;

    public String getBackdropPath() {
        return backdropPath;
    }

    public String getOverview() {
        return overview;
    }

    public String getTitle() {
        return title;
    }
}
