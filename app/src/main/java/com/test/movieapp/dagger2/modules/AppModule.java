package com.test.movieapp.dagger2.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.test.movieapp.MovieApp;

@Module
public class AppModule {
    private final MovieApp application;

    public AppModule(final MovieApp application) {
        this.application = application;
    }

    @Singleton
    @Provides
    public Application provideApplication() {
        return application;
    }
}