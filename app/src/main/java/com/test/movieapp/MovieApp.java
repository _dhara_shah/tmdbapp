package com.test.movieapp;

import android.app.Application;
import android.content.Context;

import com.test.movieapp.dagger2.components.AppComponent;
import com.test.movieapp.dagger2.components.DaggerAppComponent;
import com.test.movieapp.dagger2.components.DaggerMovieComponent;
import com.test.movieapp.dagger2.components.MovieComponent;
import com.test.movieapp.dagger2.modules.AppModule;
import com.test.movieapp.dagger2.modules.MovieAppModuleImpl;
import com.test.movieapp.di.MovieComponentProvider;
import com.test.movieapp.di.components.CoreComponent;
import com.test.movieapp.di.components.CoreComponentProvider;
import com.test.movieapp.di.components.DaggerCoreComponent;
import com.test.movieapp.di.modules.CoreModule;
import com.test.movieapp.di.modules.CoreModuleImpl;
import com.test.movieapp.di.modules.NetModule;
import com.test.movieapp.network.api.Api;

public class MovieApp extends Application implements CoreComponentProvider, MovieComponentProvider {
    public static MovieApp INSTANCE;
    private static AppComponent INJECTOR;
    private static MovieComponent MOVIE_APP_INJECTOR;
    private static volatile CoreComponent CORE_INJECTOR;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        /*Realm.init(INSTANCE);
        final RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);*/

        final CoreModule coreModule = new CoreModuleImpl(this);
        final NetModule netModule = new NetModule(Api.getHost());

        CORE_INJECTOR = DaggerCoreComponent.builder()
                .coreModule(coreModule)
                .netModule(netModule)
                .build();

        INJECTOR = DaggerAppComponent.builder()
                .coreComponent(CORE_INJECTOR)
                .appModule(new AppModule(this))
                .build();

        MOVIE_APP_INJECTOR = DaggerMovieComponent.builder()
                .coreComponent(CORE_INJECTOR)
                .movieAppModule(new MovieAppModuleImpl())
                .build();

        CORE_INJECTOR.getMovieAppLog();
    }

    @Override
    public CoreComponent getCoreComponent() {
        return CORE_INJECTOR;
    }

    @Override
    public MovieComponent getMovieComponent(final Context context) {
        return MOVIE_APP_INJECTOR;
    }
}
