package com.test.movieapp.movie.home.view;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.test.movieapp.R;
import com.test.movieapp.movie.home.model.MoviesModelAdapter;
import com.test.movieapp.movie.view.MovieDateFragment;

public class MainViewImpl implements MainView {
    private static final int MAX_SPAN_COUNT = 1;
    private boolean canLoadMore = true;
    private int visibleThreshold = 5;
    private final AppCompatActivity activity;
    private ViewInteractionListener listener;
    private MoviesAdapter moviesAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Button btnFilterByDate;
    private int totalItemCount;
    private int lastVisibleItem;
    private boolean isLoading;

    public MainViewImpl(@NonNull final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull final MoviesModelAdapter modelAdapter) {
        recyclerView = activity.findViewById(R.id.recycler_view_movies);
        progressBar = activity.findViewById(R.id.progress_bar);
        btnFilterByDate = activity.findViewById(R.id.btn_filter_date_wise);

        btnFilterByDate.setText(activity.getString(R.string.filter_by_date, ""));

        final GridLayoutManager layoutManager = new GridLayoutManager(activity, MAX_SPAN_COUNT);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        final int marginSpace = activity.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.padding_small);
        final GridSpacingItemDecoration itemDecoration = new GridSpacingItemDecoration(MAX_SPAN_COUNT, marginSpace, true);
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(final RecyclerView recyclerView, final int dx, final int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (canLoadMore) {
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (listener != null) {
                            listener.onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }
        });

        moviesAdapter = MoviesAdapter.createFrom(modelAdapter, listener);
        recyclerView.setAdapter(moviesAdapter);

        btnFilterByDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (listener != null) {
                    listener.onFilterClicked();
                }
            }
        });

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void setInteractionListener(@NonNull final ViewInteractionListener listener) {
        this.listener = listener;
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateMovies(@NonNull final MoviesModelAdapter modelAdapter) {
        btnFilterByDate.setText(activity.getString(R.string.filter_by_date, modelAdapter.getFilterDate()));
        moviesAdapter = MoviesAdapter.createFrom(modelAdapter, listener);
        recyclerView.swapAdapter(moviesAdapter, false);
        isLoading = false;
    }

    @Override
    public void endLoadMore() {
        canLoadMore = false;
    }

    @Override
    public void showDatePicker() {
        final MovieDateFragment fragment = MovieDateFragment.newInstance();
        fragment.setListener(listener);
        fragment.show(activity.getSupportFragmentManager(), "date_picker");
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(recyclerView, errorMessage, Snackbar.LENGTH_SHORT);
    }
}
