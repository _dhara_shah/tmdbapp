package com.test.movieapp.movie.home.router;

public interface MainRouter {
    void openDetails(long movieId);
}
