package com.test.movieapp.movie.moviedetails.model;

import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.entity.MovieDetailResponse;

public interface MovieDetailInteractor {
    void fetchDetail(DetailResponseListener listener);

    interface DetailResponseListener {
        void onResponseSuccess(MovieDetailResponse response);

        void onResponseError(ServiceError error);
    }
}
