package com.test.movieapp.movie.home.view;

public interface ViewInteractionListener {
    void onMoreInfoClicked(long movieId);

    void onLoadMore();

    void onFilterClicked();

    void onDateSet(String date);
}
