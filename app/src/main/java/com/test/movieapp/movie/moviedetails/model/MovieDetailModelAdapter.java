package com.test.movieapp.movie.moviedetails.model;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public interface MovieDetailModelAdapter {
    String getTitle();

    String getOverview();

    String getImageUrl();

    @Visibility
    int getOverviewVisibility();

    @IntDef({VISIBLE, INVISIBLE, GONE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Visibility {}
}
