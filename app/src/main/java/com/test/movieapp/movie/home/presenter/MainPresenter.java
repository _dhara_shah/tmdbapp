package com.test.movieapp.movie.home.presenter;

public interface MainPresenter {
    void handleOnViewCreated();
}
