package com.test.movieapp.movie.moviedetails.model;

import android.text.TextUtils;

import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieDetailResponse;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MovieDetailModelAdapterImpl implements MovieDetailModelAdapter {
    private MovieDetailResponse response;

    public MovieDetailModelAdapterImpl(MovieDetailResponse response) {
        this.response = response;
    }

    @Override
    public String getTitle() {
        return response.getTitle();
    }

    @Override
    public String getOverview() {
        return response.getOverview();
    }

    @Override
    public String getImageUrl() {
        return Api.getImageBaseUrl() + response.getBackdropPath();
    }

    @Visibility
    @Override
    public int getOverviewVisibility() {
        return TextUtils.isEmpty(getOverview()) ? GONE : VISIBLE;
    }
}
