package com.test.movieapp.movie.home.model;

public interface MoviesModelAdapter {
    int VIEW_TYPE_ROW = 1;
    int VIEW_TYPE_LOAD_MORE = 2;

    int getCount();

    String getMovieTitle(int position);

    String getDesc(int position);

    String getImageUrl(int position);

    double getAverageVote(int position);

    String getReleaseDate(int position);

    long getMovieId(int position);

    int getItemViewType(int position);

    boolean isLoadMore();

    String getFilterDate();
}
