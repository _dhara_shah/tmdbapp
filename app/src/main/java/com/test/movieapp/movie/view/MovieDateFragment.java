package com.test.movieapp.movie.view;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.test.movieapp.movie.home.view.ViewInteractionListener;

import java.util.Calendar;

public class MovieDateFragment extends DialogFragment implements OnDateSetListener {
    private ViewInteractionListener listener;

    public static MovieDateFragment newInstance() {
        return new MovieDateFragment();
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        final int yy = calendar.get(Calendar.YEAR);
        final int mm = calendar.get(Calendar.MONTH);
        final int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    @Override
    public void onDateSet(final DatePicker datePicker, final int yy, final int mm, final int dd) {
        listener.onDateSet(yy + "-" + (mm+1) + "-" + dd);
    }

    public void setListener(@Nullable final ViewInteractionListener listener) {
        this.listener = listener;
    }
}
