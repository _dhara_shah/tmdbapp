package com.test.movieapp.movie.moviedetails.view;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.movieapp.R;
import com.test.movieapp.movie.moviedetails.model.MovieDetailModelAdapter;

public class MovieDetailViewImpl implements MovieDetailView {
    private AppCompatActivity activity;
    private ProgressBar progressBar;
    private TextView txtTitle;
    private TextView txtOverview;
    private ImageView imgPoster;
    private View viewOverview;
    private Toolbar toolbar;

    public MovieDetailViewImpl(@NonNull final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews() {
        setUpActionBar();
        findViews();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateData(@NonNull final MovieDetailModelAdapter modelAdapter) {
        viewOverview.setVisibility(modelAdapter.getOverviewVisibility());
        txtOverview.setText(modelAdapter.getOverview());
        txtTitle.setText(modelAdapter.getTitle());
        Glide.with(activity).load(modelAdapter.getImageUrl()).into(imgPoster);
    }

    private void setUpActionBar() {
        toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        final ActionBar actionBar = this.activity.getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
        }
    }

    private void findViews() {
        progressBar = activity.findViewById(R.id.progress_bar);
        txtTitle = activity.findViewById(R.id.txt_movie_title);
        txtOverview = activity.findViewById(R.id.txt_movie_overview);
        imgPoster = activity.findViewById(R.id.img_poster);
        viewOverview = activity.findViewById(R.id.lbl_overview);
    }
}
