package com.test.movieapp.movie.moviedetails.presenter;

public interface MovieDetailPresenter {
    void onViewCreated();

    boolean handleOnBackPress();

    MovieDetailPresenter STUB = new MovieDetailPresenter() {
        @Override
        public void onViewCreated() {

        }

        @Override
        public boolean handleOnBackPress() {
            return true;
        }
    };
}
