package com.test.movieapp.movie.home.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.test.movieapp.R;
import com.test.movieapp.movie.home.presenter.MainPresenter;
import com.test.movieapp.movie.home.presenter.MainPresenterImpl;
import com.test.movieapp.movie.home.router.MainRouter;
import com.test.movieapp.movie.home.router.MainRouterImpl;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MainRouter router = new MainRouterImpl(this);
        final MainView view = new MainViewImpl(this);
        final MainPresenter presenter = new MainPresenterImpl(view, router);
        presenter.handleOnViewCreated();
    }
}
