package com.test.movieapp.movie.moviedetails.view;

import android.support.annotation.NonNull;

import com.test.movieapp.movie.moviedetails.model.MovieDetailModelAdapter;

public interface MovieDetailView {
    void initViews();

    void showProgress();

    void hideProgress();

    void updateData(@NonNull MovieDetailModelAdapter modelAdapter);
}
