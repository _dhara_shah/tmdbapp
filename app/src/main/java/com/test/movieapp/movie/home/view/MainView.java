package com.test.movieapp.movie.home.view;

import android.support.annotation.NonNull;

import com.test.movieapp.movie.home.model.MoviesModelAdapter;

public interface MainView {
    void initViews(@NonNull MoviesModelAdapter modelAdapter);

    void showProgress();

    void setInteractionListener(@NonNull ViewInteractionListener listener);

    void hideProgress();

    void updateMovies(@NonNull MoviesModelAdapter modelAdapter);

    void endLoadMore();

    void showDatePicker();

    void showError(String errorMessage);
}
