package com.test.movieapp.movie.home.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.entity.MovieResponse;
import com.test.movieapp.network.entity.Movies;

public interface MainInteractor {
    void fetchMovies(@NonNull ResponseListener listener, boolean firstTime);

    boolean isLoadMore();

    void setFilterDate(String filterDate);

    String getFilterDate();

    @Nullable
    List<Movies> getMovieList();

    interface ResponseListener {
        void onResponseSuccess(MovieResponse movieResponse);

        void onResponseError(ServiceError error);
    }
}
