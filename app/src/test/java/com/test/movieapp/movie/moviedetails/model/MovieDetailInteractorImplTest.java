package com.test.movieapp.movie.moviedetails.model;

import com.test.movieapp.MovieAppBaseTest;
import com.test.movieapp.movie.datasource.MainDataSource;
import com.test.movieapp.network.Listener;
import com.test.movieapp.network.ServiceError;
import com.test.movieapp.network.api.Api;
import com.test.movieapp.network.entity.MovieDetailResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@PrepareForTest(Api.class)
@RunWith(PowerMockRunner.class)
public class MovieDetailInteractorImplTest extends MovieAppBaseTest {
    private final static long MOVIE_ID = 1234;
    private MovieDetailInteractorImpl interactor;

    @Mock
    MainDataSource dataSource;
    @Mock
    MovieDetailInteractor.DetailResponseListener listener;
    @Mock
    MovieDetailResponse response;
    @Mock
    ServiceError error;
    @Captor
    ArgumentCaptor<Listener<MovieDetailResponse>> movieDetailListener;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        PowerMockito.mockStatic(Api.class);
        when(movieAppModule.provideMainDataSource()).thenReturn(dataSource);
        when(Api.getApiKey()).thenReturn("1234");
        interactor = new MovieDetailInteractorImpl(MOVIE_ID);
    }

    @Test
    public void testFetchDetailSuccess() {
        interactor.fetchDetail(listener);
        verify(dataSource).getMovieDetail(eq(MOVIE_ID), eq("1234"), movieDetailListener.capture());
        movieDetailListener.getValue().onSuccess(response);
    }

    @Test
    public void testFetchDetailError() {
        interactor.fetchDetail(listener);
        verify(dataSource).getMovieDetail(eq(MOVIE_ID), eq("1234"), movieDetailListener.capture());
        movieDetailListener.getValue().onError(error);
    }
}
